from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, VARCHAR, Text
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Members(Base):
    __tablename__ = 'members_finally'

    id = Column(Integer, primary_key=True)
    firstname = Column(VARCHAR(255), nullable=False)
    lastname = Column(VARCHAR(255), nullable=False)
    description = Column(Text(255), nullable=False)


    def __init__(self, firstname, lastname, description):
		self.firstname = firstname
		self.lastname = lastname
		self.description = description

    def __repr__(self):
		return "Members({}, {}, {})".format(self.firstname, self.lastname, self.description)

# connect to db
engine = create_engine('mysql://heraldlabial:!heraldlabial@192.168.11.248/heraldlabial')

# create database schema
Base.metadata.create_all(engine)

# CREATING THE TABLE SCHEMA
Members.__table__

# CREATING A SESSION
Session = sessionmaker(bind=engine)

s = Session()


s.add_all([Members("Emman", "Galleto", "F U 8nt scared u 8nt human"), Members("Eduy", "Gagno", "Oppa"), Members("Herald", "Labial", "Rak en roll to the world")])

s.commit()

m = s.query(Members).all()

for x in m:
	print x

